require 'vte'

class InstallerTerminal < Vte::Terminal
	type_register

	signal_new("new_lines",
			GLib::Signal::RUN_FIRST,
			nil,
			nil,
			Array)

	signal_new("line_changed",
			GLib::Signal::RUN_FIRST,
			nil,
			nil,
			String)

	def initialize(*argv)
		super(*argv)
		set_font("Monospace 10",Vte::TerminalAntiAlias::FORCE_ENABLE)
		@pos_row = 0
		@pos_col = 0
		self.signal_connect("contents-changed") {|widget| contents_track() }
	end

	def contents_track()
		now_col, now_row = self.cursor_position
		if (now_row != @pos_row)
			text = self.get_text_range(@pos_row,0,now_row,now_col,false).split("\n").collect {|x| x.strip }
			self.signal_emit("new_lines",text)
			@pos_row = now_row
			@pos_col = now_col
		end
		if (now_row == @pos_row && now_col == @pos_col)
			text = self.get_text_range(@pos_row,0,@pos_row,self.column_count,false).strip
			self.signal_emit("line_changed",text)
		end
	end

	def signal_do_line_changed(text)
		text += ""
	end

	def signal_do_new_lines(text)
		text.length
	end
end
