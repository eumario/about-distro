class SetUserCommand
	def initialize
		@args = []
		if !(`which sudo`.empty?)
			@set_user = :sudo
		else
			@set_user = :su
		end
	end

	def <<(arg)
		@args << arg
	end

	def dup
		obj = self.class.new
		@args.each { |x| obj << x }
		return obj
	end

	def format
		if @set_user == :sudo
			return ["sudo"] + @args
		else
			return ["su","-c"] << ("\"" + @args.join(" ") + "\"")
		end
	end
end
