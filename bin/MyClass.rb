
class MyClass #(change name)

	include GladeGUI

	attr_accessor :dlg_canceled, :dlg_response

	def show()
		load_glade(__FILE__)  #loads file, glade/MyClass.glade into @builder

		data = File.read("/etc/lsb-release")
		@oslabel = /DISTRIB_DESCRIPTION="([\w|\d|\s|.]+)"/.match(data)[1].to_s
		data = File.read("/proc/cpuinfo")
		@processor = /model name[\s]+:([\s|\w|\(|\)|\d|\@|.]+)^/.match(data)[1].to_s.split(" ").join(" ")
		data = File.read("/proc/meminfo")
		memtotal = /MemTotal:\s+(\d+)/.match(data)[1].to_s.to_f
		membuffer = /Buffers:\s+(\d+)/.match(data)[1].to_s.to_f
		memcache = /Cached:\s+(\d+)/.match(data)[1].to_s.to_f
		memfree = memtotal - (membuffer + memcache)
		@memory = "%0.2fGB free of %0.2fGB total" % [memfree / (1024.0 * 1024.0),
																								 memtotal / (1024.0 * 1024.0)]
		proc = `uname -m`.strip
		if proc == "i386"
			@arch = "32-bit (Intel 386)"
		elsif proc == "i686"
			@arch = "32-bit (Intel 686)"
		elsif proc == "x86_64"
			@arch = "64-bit (Generic)"
		elsif proc == "ia64"
			@arch = "64-bit (Intel)"
		elsif proc == "amd64"
			@arch = "64-bit (AMD)"
		elsif proc == "powerpc"
			@arch = "32-bit PowerPC"
		elsif proc == "ppc64"
			@arch = "64-bit PowerPC"
		else
			@arch = proc
		end

		@kernel = `uname -sr`.strip

		set_glade_all() #populates glade controls with insance variables (i.e. Myclass.label1)

		tmpimg = Gdk::Pixbuf.new("/usr/share/icons/gnome/scalable/places/start-here.svg",128,128)
		@builder["osimage"].pixbuf = tmpimg
		show_window()
	end	

	def upgrade__clicked(*argv)
		win = UpdateWindow.new
		win.show(self)
	end

	def hwinfo__clicked(*varg)
		`hardinfo &`
	end
end

