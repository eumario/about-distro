
class UpdateScriptManager

	include GladeGUI

	def show(parent)
		@parent = parent
		load_glade(__FILE__,parent)  #loads file, glade/MyClass.glade into @builder

		set_glade_all(self) #populates glade controls with insance variables (i.e. Myclass.var1) 
		show_window()
	end	

	def button1__clicked(*argv)
		if @builder["entry1"].text == ""

		end
		destroy_window()
	end

	def button2__clicked(*argv)
		@parent.dlg_canceled = true
		destroy_window()
	end

	def button3__clicked(*argv)
		dlg = Gtk::FileChooserDialog.new("Select Update Manager Script",
						@builder["window1"],
						Gtk::FileChooser::ACTION_OPEN,
						nil,
						[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
						[Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
		if dlg.run == Gtk::Dialog::RESPONSE_ACCEPT
			@builder["entry1"].text = dlg.filename
		end
		dlg.destroy
	end
end
