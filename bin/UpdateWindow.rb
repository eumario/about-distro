class UpdateWindow

	include GladeGUI
	
	attr_accessor :password
	def execute_package_manager
		cmd = SetUserCommand.new
		pacman_args = {
			:apt => {:update => "update", :upgrade => "upgrade"},
			:yum => {:upgrade => "update"},
			:pacman => {:update => "-Syy", :upgrade => "-Suu"}
		}
		pacman = 0
		
		# Figure out which Package Manager we are using
		if !(`which apt-get`.empty?)
			cmd << "apt-get"
			pacman = :apt
		elsif !(`which yum`.empty?)
			cmd << "yum"
			pacman = :yum
		elsif !(`which pacman`.empty?)
			cmd << "pacman"
			pacman = :pacman
		end

		if pacman == 0
			@terminal.feed("Unable to find a Package Manager to use!")
			return
		end
		
		# Check to see if we're using a Graphical sudo
		@stage = :update
		AskPassword.new.show(self)
		if @password.nil?
			@terminal.feed("No password given, unable to execute System Updates")
			return
		end
		if pacman == :yum
			cmd << pacman_args[pacman][:upgrade]
			@terminal.fork_command({:argv=> cmd.format})
			sleep 0.5
			@terminal.feed_child(@password + "\n")
		else
			orig = cmd.dup
			@terminal.signal_connect("child-exited") do |widget|
				if @stage == :update
					@stage = :upgrade
					cmd = orig
					cmd << pacman_args[pacman][:upgrade]
					@builder["update_progress"].text = "Sync complete, installing updates"
					@terminal.fork_command({:argv => cmd.format})
					sleep 0.5
					@terminal.feed_child(@password + "\n")
				elsif @stage == :upgrade
					@builder["update_progress"].text = "Updates complete"
					@builder["update_progress"].fraction = 1.0
				end
			end
			cmd << pacman_args[pacman][:update]
			@terminal.fork_command({:argv => cmd.format})
			sleep 0.5
			@terminal.feed_child(@password + "\n")
		end
	end

	def show(parent)
		load_glade(__FILE__, parent)
		set_glade_all() #populates glade controls with insance variables 
		@builder["terminal_container"].add(@terminal = InstallerTerminal.new)
		@builder["update_progress"].text = "Checking for Updates..."
		@builder["update_progress"].pulse
		@terminal.signal_connect("new-lines") do |lines|
			@builder["update_progress"].pulse
		end
		@terminal.signal_connect("line-changed") do |lines|
			@builder["update_progress"].pulse
		end
		execute_package_manager
		show_window()
	end

end
