
class AskPassword #(change name)

	include GladeGUI
	def show(parent)
		load_glade(__FILE__)
		@parent = parent
		set_glade_all() #populates glade controls with insance variables 
		show_window()
	end

	def button_ok__clicked(*argv)
		@parent.password = @builder["password_entry"].text
		destroy_window()
	end

	def button_cancel__clicked(*argv)
		@parent.password = nil
		destroy_window()
	end
end
